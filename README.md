Send notes front
===

WIP

Instructions
---

Run with:
```
	npm start
```

- [Koajs](https://www.npmjs.com/package/koa)

- [Koa-views](https://www.npmjs.com/package/koa-views)
- [Nunjucks](https://www.npmjs.com/package/nunjucks)

Why?
---

I like building systems in my head and notes don't have the ability to store data in the same way as my mind does      
so hopefully i'm able to finish this but i'm wasting a lot of time writing this readme so i'm not so sure

Information stored in notes is very one dimensional and it's easy to lose important information in the stream of less useful boilerplate text.
I'd like to add functionality to notes so you're able to build a relational database-y backup of them, so it's more easy to find and build upon existing knowledge.    

Goal
---

Use information in notes to construct note objects or nobs for short

something like:

In note:    
So i met a new person at work his name is john,    
his hobbies are hiking and he has a friend called sarah and another one called dom

work->new person->name:'john',new hobby:'hiking',friend:'sarah','dom'

To json:    
```js
work{
  person[
	  {
	    "name":"john",
	    "hobby":[
		      "hiking"
		    ] 
	  }
  ]
}
```

Not sure how to do this yet but it's probably going to be the easiest way    

Unrelated
---
Discovered music:
Dream Theater (group)