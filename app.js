const Koa = require('koa');
const PrettifyJson = require('koa-json');
const BodyParser = require('koa-bodyparser');
const Router = require('koa-router');
const Views = require('koa-views');
const Nunjucks = require('nunjucks');

//const Passport = require('koa-passport');
//const Session = require('koa-session');


const app = new Koa();
const router = new Router();
//app.keys = ['secret'];

// Port (for heroku)
const PORT = process.env.PORT || 3001;

/* NUNJUCKS CONFIG */
Nunjucks.configure({autoescape:true});
const nunjucksSettings = {
    options: {
      settings: {
        views: __dirname + '/views'
      }
    },
    extension: 'njk',
    map: {
      njk: 'nunjucks'
    }
};

/* MIDDLEWARE */
app
    .use(PrettifyJson())
    .use(BodyParser())
    .use(Views(__dirname + '/views',nunjucksSettings))
    .use(router.routes())
    .use(router.allowedMethods());
    //.use(Session({},app))
    //.use(Passport.initialize())
    //.use(Passport.session())

/* PAGES */
const indexPage = {
    page:{
        title: 'heythisworks',
        body: 'something didnt go wrong'
    }
};

/* EVENTS */



/* ROUTES */
router.get('/',async ctx => {
    await ctx.render('index', indexPage);
})

app.listen(PORT,()=>{console.log('Listening on port '+PORT)});